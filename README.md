# Assignment 2 - Password Vault
**Note:** This project is implemented as a maven project. To set up the project, you can use Eclipse's Maven plugin.

The tests can be validated simply by running the following:
```bash
mvn clean test
```

## Is your solution fully working or not?
* Yes, my solution is fully working

## Do you believe your unit tests to be complete to test the module's contract?  If not, why not?
* Yes. Each unit test tests a specific subset of the functionality of the implemented method.
* Various configurations of data are attempted on each method call, and the results are tested against specific expectations.

## What extra credit problem(s) did you work on?  If so, how do you demonstrate the functionality?
* No extra credit

## How much time did you spend on the assignment?
* 6-7 hours

## Overall how did you like the homework in terms of
### Applying concepts discussed in class
* I felt the homework involved so many technical details that it distracted from the purpose of the assignment

### Improving your skills as a software developer
* I implemented this assignment using a test-driven development approach; it was a good opportunity to gain confidence in this methodology since the Exception types were well defined and drove the testing and implemention
* I enjoyed working with the interface stubs and scaffolding provided in the assignment framework