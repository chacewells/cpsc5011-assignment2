package vault;

/**
 * A plaintext token with the following characteristics:
 *  - 6 <= length <= 12
 *  - Consists entirely of lowercase alphabetic ASCII characters (a - z)
 */
public interface PlaintextToken {
    /**
     * A pattern that validates the above mentioned invariants.
     */
	String VALID_USERNAME_LETTERS_RE = "^[a-z]{6,12}$";

    /**
     * The string backing this plaintext token.
     * @return The token
     */
	String getToken();

    /**
     * @return True if the token meets the class invariants; false otherwise.
     */
	default boolean isValid() {
		String token = getToken();
		
		return token.matches(VALID_USERNAME_LETTERS_RE);
	}
}
