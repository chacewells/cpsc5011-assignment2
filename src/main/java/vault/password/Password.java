package vault.password;

import encrypt.CaesarCypher;
import encrypt.Encryptor;
import vault.InvalidPasswordException;

import java.util.*;

/**
 * Represents a password token.
 * The plaintext password token must meet the following requirements:
 * - Must contain at least one of the following special characters: !@#$%^&
 * - Must contain at least one uppercase letter
 * - Must contain at least one lowercase letter
 *
 * The password is stored in memory in its encrypted form. This cryptographic transformation is described by
 * {@link CaesarCypher}
 */
public class Password {
	public static final int MIN_LENGTH = 6;
	public static final int MAX_LENGTH = 15;
	public static final Collection<Character> SPECIAL_CHARACTERS = Collections.unmodifiableCollection(
	        new HashSet<>(Arrays.asList('!', '@', '#', '$', '%', '^', '&'))
    );
	public static final String CONTAINS_LETTER_RE = ".*[A-Za-z].*";
	public static final String CONTAINS_DIGIT_PATTERN_RE = ".*[0-9].*";

	private String encryptedPassword;

    /**
     * Constructs a new password token, validates the plaintext password, and stores it in encrypted form.
     *
     * @param plaintext The plaintext password.
     * @throws InvalidPasswordException If the password does not meet the requirements detailed in the class JavaDoc
     */
	public Password(String plaintext) throws InvalidPasswordException {
		validatePlaintext(plaintext);

		encryptedPassword = encrypt(plaintext);
	}

    /**
     * @return The encrypted password.
     */
	public String getEncryptedPassword() {
	    return encryptedPassword;
    }

    // helper function to encrypt the plaintext
	private static String encrypt(String plaintext) {
        Encryptor cipher = new CaesarCypher();
        return cipher.encrypt(plaintext);
    }

    // Throws an exception if the plaintext is not a valid password
	private static void validatePlaintext(String plaintext) throws InvalidPasswordException {
	    if (!isValidPassword(plaintext))
	        throw new InvalidPasswordException();
	}

	// Checks the plaintext against the password requirements
	private static boolean isValidPassword(String plaintext) {
		int length = plaintext.length();

		return MIN_LENGTH <= length
            && length <= MAX_LENGTH
            && containsSpecial(plaintext)
            && plaintext.matches(CONTAINS_LETTER_RE)
            && plaintext.matches(CONTAINS_DIGIT_PATTERN_RE);
	}

	// Whether the given string contains a required special character
	private static boolean containsSpecial(String plaintext) {
        for (char c: plaintext.toCharArray())
            if (SPECIAL_CHARACTERS.contains(c))
                return true;

        return false;
    }

    @Override
    public int hashCode() {
        return encryptedPassword.hashCode() - 1;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        if (obj == this)
            return true;

        if (obj.hashCode() != hashCode())
            return false;

        if (obj instanceof Password) {
            Password other = (Password)obj;
            return encryptedPassword.equals(other.encryptedPassword);
        } else {
            return false;
        }
    }
}
