package vault.password;

import vault.InvalidPasswordException;

import java.util.Random;

public class RandomPassword {

    private String plaintext;
    private Password encrypted;

    public RandomPassword() {
        initialize();
    }

    private void initialize() {
        plaintext = generatePlaintext();
        try {
            encrypted = new Password(plaintext);
        } catch (InvalidPasswordException e) { // this shouldn't happen
            throw new RuntimeException(e);
        }
    }

    private static String generatePlaintext() {
        final int passwordLengthRange = Password.MAX_LENGTH - Password.MIN_LENGTH + 1;

        Random rand = new Random();

        final int passwordLength = rand.nextInt(passwordLengthRange) + Password.MIN_LENGTH;
        final int specialPosition = rand.nextInt(passwordLength);
        int digitPosition;
        do { // ensure digit and special char don't end up at the same position
            digitPosition = rand.nextInt(passwordLength);
        } while (digitPosition == specialPosition);

        StringBuilder plaintext = new StringBuilder();

        for (int i = 0; i < passwordLength; ++i) {
            if (i == specialPosition) {
                plaintext.append(randomSpecial(rand));
            } else if (i == digitPosition) {
                plaintext.append(randomDigit(rand));
            } else {
                plaintext.append(randomLetter(rand));
            }
        }

        return plaintext.toString();
    }

    private static char randomLetter(Random rand) {
        final int uppercaseLowerBound = 'A';
        final int lowercaseLowerBound = 'a';
        final int range = 'Z' - 'A' + 1;

        boolean coin = rand.nextBoolean();
        int position = rand.nextInt(range);

        return (char)(position + (coin ? uppercaseLowerBound : lowercaseLowerBound));
    }

    private static char randomSpecial(Random rand) {
        final char[] specialChars = "!@#$%^&".toCharArray();
        return specialChars[rand.nextInt(specialChars.length)];
    }

    private static String randomDigit(Random rand) {
        final int digitRange = 10;
        return String.valueOf(rand.nextInt(digitRange));
    }

    public String getPlaintext() {
        return plaintext;
    }

    public Password getEncrypted() {
        return encrypted;
    }

}
