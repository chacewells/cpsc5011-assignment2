package vault;

import java.util.HashMap;
import java.util.Map;

import encrypt.CaesarCypher;
import encrypt.Encryptor;
import vault.password.Password;
import vault.password.RandomPassword;
import vault.site.Site;
import vault.site.SiteName;
import vault.user.User;
import vault.user.Username;

/**
 * Implementation of {@link VaultInterface}
 * @author wellsaaron
 */
public class Vault implements VaultInterface {
	
	// users map, keyed by username
	private Map<String, User> users = new HashMap<>();

	@Override
	public void newUser(String username, String password)
			throws InvalidUsernameException, InvalidPasswordException, DuplicateUserException {
		Username usernameToken = new Username(username);
        Password passwordToken = new Password(password);

		User newUser = new User(usernameToken, passwordToken);

		if (userExists(username))
		    throw new DuplicateUserException();

		users.put(newUser.getUsername(), newUser);
	}
	
	@Override
	public String newSite(String username, String password, String sitename) throws DuplicateSiteException,
			UserNotFoundException, UserLockedOutException, PasswordMismatchException, InvalidSiteException {
        if (!userExists(username)) {
            throw new UserNotFoundException();
        }

        User user = users.get(username);
        user.authenticate(password);

        RandomPassword randomPassword = new RandomPassword();
        Password encryptedPassword = randomPassword.getEncrypted();

        SiteName newSitename = new SiteName(sitename);
        Site newSite = new Site(newSitename, encryptedPassword);

        user.addSite(newSite);

        return randomPassword.getPlaintext();
    }

	@Override
	public String updateSite(String username, String password, String sitename)
			throws SiteNotFoundException, UserNotFoundException, UserLockedOutException, PasswordMismatchException {
        if (!userExists(username))
            throw new UserNotFoundException();

        User user = users.get(username);
        user.authenticate(password);

        RandomPassword randomPassword = new RandomPassword();
        Password encryptedPassword = randomPassword.getEncrypted();

        user.updateSite(sitename, encryptedPassword);

        return randomPassword.getPlaintext();
	}

	@Override
	public String retrieveSite(String username, String password, String sitename)
			throws SiteNotFoundException, UserNotFoundException, UserLockedOutException, PasswordMismatchException {
        if (!userExists(username))
            throw new UserNotFoundException();

        User user = users.get(username);
        user.authenticate(password);

        if (!user.hasSite(sitename))
            throw new SiteNotFoundException();

        String decrypted = user.getSite(sitename).getDecryptedPassword();

        return decrypted;
    }

    /**
     * Encryptor access for backdoor testing.
     *
     * @return the {@link Encryptor} implementation this Vault uses.
     */
	public Encryptor getEncryptor() {
	    return new CaesarCypher();
    }

    /**
     * The user's encrypted password for the given site.
     *
     * @param username The user's username.
     * @param sitename The sitename for which to retrieve the encrypted password.
     * @return The encrypted password for the given site if the given user and site exist; otherwise null.
     */
    public String getEncryptedPassword(String username, String sitename) {
	    if (userExists(username)) {
	        User user = users.get(username);
	        if (user.hasSite(sitename))
                return user.getSite(sitename).getEncryptedPassword();
        }

        return null;
    }

    // Whether the user exists in this vault.
    boolean userExists(String username) {
        return users.containsKey(username);
    }

}
