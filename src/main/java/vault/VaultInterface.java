package vault;

/*
  In addition the Vault must implement two constructors
  
	public Vault();
	public Vault(encrypt.Encryptor e);
	
  The vault must also implement the following two methods for
  debugging / testing purposes.  Note that these methods
  have package-level visibility
  
  String getEncryptedPassword(String username, String sitename)
     throws UserNotFountException, SiteNotFoundException;
  Encryptor getEncryptor();
*/

public interface VaultInterface {
	/**
	 * Creates a new user with the given credentials.
	 *
	 * @param username The user to add - See {@link vault.PlaintextToken} for validation rules.
	 * @param password The user's new password - See {@link vault.password.Password} for validation rules.
	 * @throws InvalidUsernameException If the username fails validation.
	 * @throws InvalidPasswordException If the password fails validation.
	 * @throws DuplicateUserException If the user already exists.
	 */
	public void newUser(String username, String password) throws InvalidUsernameException,
			InvalidPasswordException, DuplicateUserException;

	/**
	 * Authenticates the user's credentials. If authentication succeeds, creates a random plaintext password for the
	 * site, stores it encrypted, and returns the plaintext password to the user.
	 *
	 * @param username The user's username.
	 * @param password The user's password.
	 * @param sitename The site for which to create a password - See {@link PlaintextToken} for validation rules.
	 * @return The randomly generated plaintext password.
	 * @throws DuplicateSiteException If the site already exists.
	 * @throws UserNotFoundException If the user does not exist.
	 * @throws UserLockedOutException If this is the 3rd or greater subsequent failed login attempt.
	 * @throws PasswordMismatchException If the user fails authentication.
	 * @throws InvalidSiteException If the site is not found.
	 */
	public String newSite(String username, String password, String sitename)
			throws DuplicateSiteException, UserNotFoundException,
			UserLockedOutException, PasswordMismatchException, InvalidSiteException;

    /**
     * Authenticates the user's credentials. If authentication succeeds, updates the given site with a new plaintext
     * password for the site, stores it encrypted, and returns the plaintext password to the user.
     * @param username The user's username.
     * @param password The user's password.
     * @param sitename The site to update.
     * @return The randomly generated plaintext password.
     * @throws SiteNotFoundException If the site does not exist.
     * @throws UserNotFoundException If the user does not exist.
     * @throws UserLockedOutException If this is the 3rd or greater subsequent failed login attempt.
     * @throws PasswordMismatchException If the user fails authentication.
     */
	public String updateSite(String username, String password, String sitename)
			throws SiteNotFoundException, UserNotFoundException,
			UserLockedOutException, PasswordMismatchException;

    /**
     * Authenticates the user's credentials. If authentication succeeds, retrieves and returns the user's password for
     * that site in plaintext.
     *
     * @param username The user's username.
     * @param password The user's password.
     * @param sitename The site to update.
     * @return The plaintext password.
     * @throws SiteNotFoundException If the site does not exist.
     * @throws UserNotFoundException If the user does not exist.
     * @throws UserLockedOutException If this is the 3rd or greater subsequent failed login attempt.
     * @throws PasswordMismatchException If the user fails authentication.
     */
	public String retrieveSite(String username, String password, String sitename)
			throws SiteNotFoundException, UserNotFoundException,
			UserLockedOutException, PasswordMismatchException;
}
