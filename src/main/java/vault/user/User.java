package vault.user;

import java.util.HashMap;
import java.util.Map;

import vault.*;
import vault.password.Password;
import vault.site.Site;

/**
 * Represents a user in the password vault.
 * Class invariants:
 * - Username meets {@link PlaintextToken} invariants
 * - Password meets {@link Password} invariants.
 * - Attempting to add a site that already exists results in an exception.
 * - Attempting to update a site that does not exist results in an exception.
 * - Sites that have been added chan be retrieved.
 * - If 3 consecutive authentication attempts have been made on this user, no further attempts can be made.
 */
public class User {
    /**
     * The maximum authentication attempts that can be made before this user is locked.
     */
    public static int MAX_AUTHENTICATION_ATTEMPTS = 3;

    // The number of times the user has attempted to login consecutively.
    private int consecutiveAuthenticationAttempts;

	private Username username; // the username
	private Password vaultPassword; // the password; stored encrypted
	// sites, keyed by sites name
	private Map<String, Site> sites = new HashMap<>();

    /**
     * Creates a new user with the given credentials.
     * @param username The username.
     * @param vaultPassword The encrypted password.
     */
	public User(Username username, Password vaultPassword) {
		this.username = username;
		this.vaultPassword = vaultPassword;
		resetAuthenticationAttempts();
	}

    /**
     * @return The username.
     */
	public String getUsername() {
		return username.getToken();
	}

    /**
     * Adds a site to this database, failing if the site already exists.
     * @param site The site to add.
     * @throws DuplicateSiteException If the site is already associated with the user.
     */
	public void addSite(Site site) throws DuplicateSiteException {
	    if (hasSite(site.getName()))
	        throw new DuplicateSiteException();
        sites.put(site.getName(), site);
    }

    /**
     * Updates the site with the given password.
     * @param sitename The existing sitename.
     * @param password The new password.
     * @throws SiteNotFoundException
     */
    public void updateSite(String sitename, Password password) throws SiteNotFoundException {
	    if (!hasSite(sitename))
	        throw new SiteNotFoundException();

	    Site site = sites.get(sitename);
	    site.setPassword(password);
    }

    /**
     * Whether the given site exists for the user.
     * @param sitename The site name.
     * @return Whether this user has an associated site with that of the given name.
     */
    public boolean hasSite(String sitename) {
	    return sites.containsKey(sitename);
    }

    /**
     * Retrieves the site from this user.
     * @param sitename The sitename.
     * @return The site associated with the given name.
     */
    public Site getSite(String sitename) {
	    return sites.get(sitename);
    }

    /**
     * Verifies the identity of this user by checking the given password against that on record for this user. The
     * attempts count will reset on success.
     * @param attempt The password to authenticate with.
     * @throws PasswordMismatchException On consecutive attempts fewer than 3 where the authentication fails.
     * @throws UserLockedOutException On the user's 3rd consecutive failed login attempt.
     */
    public void authenticate(String attempt) throws PasswordMismatchException, UserLockedOutException {
	    if (consecutiveAuthenticationAttempts >= MAX_AUTHENTICATION_ATTEMPTS) {
	        ++consecutiveAuthenticationAttempts;
            throw new UserLockedOutException();
        }

        try {
            Password password = new Password(attempt);
            if (!vaultPassword.equals(password))
                loginFailed();
        } catch (InvalidPasswordException e) {
            loginFailed();
        }

        resetAuthenticationAttempts();
    }

    // increments the authentication failure count and throws a PasswordMismatchException
    private void loginFailed() throws PasswordMismatchException {
	    ++consecutiveAuthenticationAttempts;
	    throw new PasswordMismatchException();
    }

    // resets the login attempts count to 0
    private void resetAuthenticationAttempts() {
        consecutiveAuthenticationAttempts = 0;
    }

}
