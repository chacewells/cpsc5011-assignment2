package vault.user;

import vault.InvalidUsernameException;
import vault.PlaintextToken;

/**
 * A username. Simply a PlaintextToken, but the type represents a username.
 */
public class Username implements PlaintextToken {
	// the username this token represents.
	private String username;

	/**
	 * Constructs a new username and validates that PlaintextToken invariants are met.
	 * @param username The username.
	 * @throws InvalidUsernameException If the username fails PlaintextToken invariants.
	 */
	public Username(String username) throws InvalidUsernameException {
		this.username = username;
		validate();
	}

	@Override
	public String getToken() {
		return username;
	}

	// Validates the username token.
	private void validate() throws InvalidUsernameException {
		if (!isValid())
			throw new InvalidUsernameException();
	}
	
}
