package vault.site;

import vault.PlaintextToken;

/**
 * A site name. Simply a PlaintextToken, but the type represents a sitename.
 */
public class SiteName implements PlaintextToken {
    // the sitename backing this token
	private String siteName;

    /**
     * Constructs a enw sitename.
     * 	 * @param sitename The sitename.
     * @param siteName
     */
	public SiteName(String siteName) {
		this.siteName = siteName;
	}

	@Override
	public String getToken() {
		return siteName;
	}
}
