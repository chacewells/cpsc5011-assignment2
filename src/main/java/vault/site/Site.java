package vault.site;

import encrypt.CaesarCypher;
import encrypt.Encryptor;
import vault.InvalidSiteException;
import vault.password.Password;

public class Site {
    private SiteName name;
    private Password password;

    public Site(SiteName name, Password password) throws InvalidSiteException {
        this.name = name;
        this.password = password;
        validate();
    }

    private void validate() throws InvalidSiteException {
        if (!name.isValid())
            throw new InvalidSiteException();
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public String getName() {
        return name.getToken();
    }

    public String getEncryptedPassword() {
        return password.getEncryptedPassword();
    }

    public String getDecryptedPassword() {
        Encryptor cipher = new CaesarCypher();
        String decryptedPassword = cipher.decrypt(password.getEncryptedPassword());

        return decryptedPassword;
    }
	
}
