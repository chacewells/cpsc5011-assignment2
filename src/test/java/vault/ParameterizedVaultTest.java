package vault;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Collection;
import java.util.LinkedList;

@RunWith(Parameterized.class)
public class ParameterizedVaultTest extends VaultTestCommon {
	@Parameters
	public static Collection<Object[]> data() {
		Collection<Object[]> parameters =  new LinkedList<>();
		
		parameters.add(new Object[] { "boofi", "sh0r!", "short" });
		parameters.add(new Object[] {
		        "manamigonnagetintroubleforthis",
                "thi$passwordin5spiteofContainingAllcharactersIsTooLong",
                "ireallyamgonnagetintroubleforthis"
		});
		parameters.add(new Object[] { "Alphino", "nonumber$", "$niper" });
		parameters.add(new Object[] { "!@#$%^", "!@#$%^", "!@#$%" });
		
		return parameters;
	}
		
	public ParameterizedVaultTest(String badUsername, String badPassword, String badSitename) {
		this.badUsername = badUsername;
		this.badPassword = badPassword;
		this.badSitename = badSitename;
	}

	private final String badUsername;
	private final String badPassword;
	private final String badSitename;
	
	@Test(expected = InvalidUsernameException.class)
	public void testBadUsername()
			throws InvalidUsernameException, InvalidPasswordException, DuplicateUserException {
		Vault vault = blankVault();
		vault.newUser(badUsername, validPassword);
	}

	@Test(expected = InvalidPasswordException.class)
	public void testBadPassword()
		throws InvalidUsernameException, InvalidPasswordException, DuplicateUserException {
		Vault vault = blankVault();
		vault.newUser(validUsername, badPassword);
	}

	@Test(expected = InvalidSiteException.class)
    public void testNewSiteInvalidSite()
            throws UserNotFoundException, UserLockedOutException, PasswordMismatchException,
            InvalidSiteException, DuplicateSiteException {
	    Vault vault = vaultWithValidUser();
	    vault.newSite(validUsername, validPassword, badSitename);
    }


}
