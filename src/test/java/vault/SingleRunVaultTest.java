package vault;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SingleRunVaultTest extends VaultTestCommon {

    private final String wrongPassword = "deadWrong5%";

    @Test(expected = DuplicateSiteException.class)
    public void testNewSiteDuplicate()
            throws UserNotFoundException, UserLockedOutException, PasswordMismatchException,
            InvalidSiteException, DuplicateSiteException {
        Vault vault = vaultWithValidUserAndValidSite();
        vault.newSite(validUsername, validPassword, validSitename);
    }

    @Test(expected = UserNotFoundException.class)
    public void testNewSiteUserNotFound()
            throws UserNotFoundException, UserLockedOutException, PasswordMismatchException,
            InvalidSiteException, DuplicateSiteException {
        Vault vault = blankVault();
        vault.newSite(validUsername, validPassword, validSitename);
    }

    @Test(expected = PasswordMismatchException.class)
    public void testNewSitePasswordMismatch()
            throws UserNotFoundException, UserLockedOutException, PasswordMismatchException,
            InvalidSiteException, DuplicateSiteException {
        Vault vault = vaultWithValidUser();
        vault.newSite(validUsername, wrongPassword, validSitename);
    }

    @Test(expected = UserLockedOutException.class)
    public void testNewSiteUserLockedOut()
            throws UserNotFoundException, UserLockedOutException,
            InvalidSiteException, DuplicateSiteException {
        final int authenticationAttempts = 4;
        Vault vault = vaultWithValidUser();
        for (int i = 1; i <= authenticationAttempts; ++i) {
            try {
                vault.newSite(validUsername, wrongPassword, validSitename);
            } catch (PasswordMismatchException e) {
                System.out.println("Authentication attempt " + i + " failed. Retry login.");
            }
        }
    }

    @Test
    public void testUpdateSite() throws UserNotFoundException, UserLockedOutException, PasswordMismatchException,
            SiteNotFoundException, InvalidSiteException, DuplicateSiteException {
        Vault vault = vaultWithValidUserAndValidSite();
        String plaintext = vault.updateSite(validUsername, validPassword, validSitename);

        assertEquals(
                vault.getEncryptor().encrypt(plaintext),
                vault.getEncryptedPassword(validUsername, validSitename));
    }

    @Test(expected = SiteNotFoundException.class)
    public void testUpdateSiteNotFound() throws UserNotFoundException, UserLockedOutException,
            PasswordMismatchException, SiteNotFoundException {
        Vault vault = vaultWithValidUser();
        vault.updateSite(validUsername, validPassword, validSitename);
    }

    @Test(expected = UserNotFoundException.class)
    public void testUpdateSiteUserNotFound() throws UserNotFoundException, UserLockedOutException,
            PasswordMismatchException, SiteNotFoundException {
        Vault vault = blankVault();
        vault.updateSite(validUsername, validPassword, validSitename);
    }

    @Test(expected = PasswordMismatchException.class)
    public void testUpdateSitePasswordMismatch() throws UserNotFoundException, UserLockedOutException,
            PasswordMismatchException, InvalidSiteException, DuplicateSiteException, SiteNotFoundException {
        Vault vault = vaultWithValidUserAndValidSite();
        vault.updateSite(validUsername, wrongPassword, validSitename);
    }

    @Test(expected = UserLockedOutException.class)
    public void testUpdateSiteUserLockedOut() throws UserNotFoundException, UserLockedOutException,
            PasswordMismatchException, InvalidSiteException, DuplicateSiteException, SiteNotFoundException {
        final int authenticationAttempts = 4;
        Vault vault = vaultWithValidUserAndValidSite();
        for (int i = 1; i <= authenticationAttempts; ++i) {
            try {
                vault.updateSite(validUsername, wrongPassword, validSitename);
            } catch (PasswordMismatchException e) {
                System.out.println("Authentication attempt " + i + " failed. Retry login.");
            }
        }
    }

    @Test
    public void testRetrieveSite() throws UserNotFoundException, UserLockedOutException, PasswordMismatchException,
            SiteNotFoundException, DuplicateSiteException, InvalidSiteException {
        Vault vault = vaultWithValidUser();
        String plaintext = vault.newSite(validUsername, validPassword, validSitename);
        String encryptedPassword = vault.retrieveSite(validUsername, validPassword, validSitename);

        assertEquals(
                plaintext,
                encryptedPassword);
    }

    @Test(expected = SiteNotFoundException.class)
    public void testRetrieveSiteNotFound() throws UserNotFoundException, UserLockedOutException,
            PasswordMismatchException, SiteNotFoundException {
        Vault vault = vaultWithValidUser();
        vault.retrieveSite(validUsername, validPassword, validSitename);
    }

    @Test(expected = UserNotFoundException.class)
    public void testRetrieveSiteUserNotFound() throws UserNotFoundException, UserLockedOutException,
            PasswordMismatchException, SiteNotFoundException {
        Vault vault = blankVault();
        vault.retrieveSite(validUsername, validPassword, validSitename);
    }

    @Test(expected = UserLockedOutException.class)
    public void testRetrieveSiteUserLockedOut() throws UserNotFoundException, UserLockedOutException,
            PasswordMismatchException, InvalidSiteException, DuplicateSiteException, SiteNotFoundException {
        final int authenticationAttempts = 4;
        Vault vault = vaultWithValidUserAndValidSite();
        for (int i = 1; i <= authenticationAttempts; ++i) {
            try {
                vault.retrieveSite(validUsername, wrongPassword, validSitename);
            } catch (PasswordMismatchException e) {
                System.out.println("Authentication attempt " + i + " failed. Retry login.");
            }
        }
    }

    @Test(expected = PasswordMismatchException.class)
    public void testRetrieveSitePasswordMismatch() throws UserNotFoundException, UserLockedOutException,
            PasswordMismatchException, InvalidSiteException, DuplicateSiteException, SiteNotFoundException {
        Vault vault = vaultWithValidUserAndValidSite();
        vault.retrieveSite(validUsername, wrongPassword, validSitename);
    }

    @Test
    public void testNewSiteUserExistsValidSitename()
            throws UserNotFoundException, UserLockedOutException, PasswordMismatchException,
            InvalidSiteException, DuplicateSiteException {
        Vault vault = vaultWithValidUser();
        String plaintext = vault.newSite(validUsername, validPassword, validSitename);

        assertEquals(
                vault.getEncryptor().encrypt(plaintext),
                vault.getEncryptedPassword(validUsername, validSitename));
    }

    @Test
    public void testNewUserValidCredentials()
            throws InvalidUsernameException, InvalidPasswordException, DuplicateUserException {
        Vault vault = blankVault();
        vault.newUser(validUsername, validPassword);

        assertTrue("User exists", vault.userExists(validUsername));
    }

}
