package vault;

import static org.junit.Assert.fail;

public class VaultTestCommon {

    final String validUsername = "alphino";
    final String validPassword = "aB1!@#$%^&";
    final String validSitename = "boosters";

    Vault blankVault() {
        return new Vault();
    }

    Vault vaultWithValidUser() {
        Vault vault = blankVault();
        try {
            vault.newUser(validUsername, validPassword);
        } catch (InvalidUsernameException|InvalidPasswordException|DuplicateUserException e) {
            fail("Vault should initialize containing a valid user.");
        }

        return vault;
    }

    Vault vaultWithValidUserAndValidSite()
            throws UserNotFoundException, UserLockedOutException, PasswordMismatchException,
            InvalidSiteException, DuplicateSiteException {
        Vault vault = vaultWithValidUser();
        vault.newSite(validUsername, validPassword, validSitename);

        return vault;
    }
}
